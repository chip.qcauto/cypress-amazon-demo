<p align="center">
A demonstration of <strong>Automation</strong> usage of <a href="https://cypress.io">Cypress</a> testing methods.
</p>


> 💬 **Note from interviewee**
>
> Because of Amazon login rate limit, this demostration doesn't have any login successfully test cases.
>
---

### Installation

```shell
npm ci
```

### Start Cypress

```shell
node_module/.bin/cypress open
```

### Gitlab-CI
```shell
https://gitlab.com/chip.qcauto/cypress-amazon-demo/-/jobs
```

### Sample report
```shell
- Open https://gitlab.com/chip.qcauto/cypress-amazon-demo/-/pipelines/182526482/test_report
- Click on job for detailed report
```