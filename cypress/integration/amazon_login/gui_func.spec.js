/// <reference types="cypress" />

import loginPage from '../../pageObjects/login.page'


describe('GUI & Functionality', function () {

    before(() => {
        cy.fixture('email.json').then((data) => {
            this.email = data
        })
    })

    beforeEach(() => {
        cy.visit(Cypress.env('host') + Cypress.env('login-page'))
    })

    it('[SIAL-1] Input incorrect email', () => {
        loginPage
            .fillInEmail(this.email.incorrectEmail)
            .pressContinue()
            .elementTextContentShouldBe(
                loginPage.errorAlert,
                loginPage.incorrectEmailText
            )
    })

    it("[SIAL-2] Don't input email", () => {
        loginPage
            .fillInEmail("")
            .pressContinue()
            .elementTextContentShouldBe(
                loginPage.missingEmailAlert,
                loginPage.missingEmailText
            )
    })

    it("[SIAL-3] Input un-registered email", () => {
        loginPage
            .fillInEmail(this.email.unregisteredEmail)
            .pressContinue()
            .elementTextContentShouldBe(
                loginPage.errorAlert,
                loginPage.incorrectEmailText
            )
    })

    it("[SIAL-4] Don't input password", () => {
        loginPage
            .fillInEmail(this.email.registeredEmail)
            .pressContinue()
            .pressSignIn()
            .elementTextContentShouldBe(
                loginPage.missingPasswordAlert,
                loginPage.missingPasswordText
            )
    })

    it("[SIAL-5] Password textbox is not visible", () => {
        loginPage
            .fillInEmail(this.email.registeredEmail)
            .elementShouldNotBeVisible(loginPage.passwordTextbox)
    })
    

    it("[SIAL-6] Input incorrect password", () => {
        loginPage
            .fillInEmail(this.email.registeredEmail)
            .pressContinue()
            .fillInPassword(this.email.incorrectPassword)
            .pressSignIn()
            .elementTextContentShouldBe(
                loginPage.warningAlert,
                loginPage.warningText
            )
    })

    it("[SIAL-7] Don't input password", () => {
        loginPage
            .fillInEmail(this.email.registeredEmail)
            .pressContinue()
            .pressSignIn()
            .elementTextContentShouldBe(
                loginPage.missingPasswordAlert,
                loginPage.missingPasswordText
            )
    })

    it("[SIAL-8] Email textbox is not visible", () => {
        loginPage
            .fillInEmail(this.email.registeredEmail)
            .pressContinue()
            .elementShouldNotBeVisible(loginPage.emailTextbox)
    })

})