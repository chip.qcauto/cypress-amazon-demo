import { basePage } from './base.page'

export default new class loginPage extends basePage {

    //element locators
    emailTextbox = '#ap_email'
    continueButton = '#continue'
    passwordTextbox = '#ap_password'
    signInButton = '#signInSubmit'
    missingEmailAlert = '#auth-email-missing-alert > .a-box-inner > .a-alert-content'
    errorAlert = '#auth-error-message-box > .a-box-inner > .a-alert-content'
    missingPasswordAlert = '#auth-password-missing-alert > .a-box-inner > .a-alert-content'
    warningAlert = '#auth-warning-message-box > .a-box-inner > .a-alert-content'

    //element text contents
    missingEmailText = 'Enter your email or mobile phone number'
    incorrectEmailText = 'We cannot find an account with that email address'
    missingPasswordText = 'Enter your password'
    incorrectPasswordText = 'Your password is incorrect'
    warningText = 'To better protect your account, please re-enter your password and then enter the characters as they are shown in the image below.'

    fillInEmail(email) {
        this.fillIn(this.emailTextbox, email)
        return this
    }

    pressContinue() {
        cy.get(this.continueButton).click()
        return this
    }

    fillInPassword(password) {
        this.fillIn(this.passwordTextbox, password)
        return this
    }

    pressSignIn() {
        cy.get(this.signInButton).click()
        return this
    }

}