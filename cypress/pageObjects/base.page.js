export class basePage {

    fillIn(element, content) {
        cy.get(element).clear()
        if (content)
            cy.get(element).type(content)
    }

    elementTextContentShouldBe(element, expectedContent) {
        cy.get(element).invoke('text').should(actual => {
            expect(actual).to.contain(expectedContent)
        })
        return this
    }

    elementShouldNotBeVisible(element) {
        cy.get(element).should("not.be.visible");
    }

}